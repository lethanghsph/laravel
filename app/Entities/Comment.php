<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment.
 *
 * @package namespace App\Entities;
 */
class Comment extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_ACTIVE  = 2;
    const STATUS_REFUSE  = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
    ];

    public $timestamps = false;

    // "polymorphic" relationship.
    public function commentable()
    {
        return $this->morphTo();
    }

}
