<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product.
 *
 * @package namespace App\Entities;
 */
class Product extends Model
{
    const STATUS_DRAF    = 0;
    const STATUS_PENDING = 1;
    const STATUS_ACTIVE  = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'thumbnail',
        'content',
        'price',
    ];

    // "One-many" relationship.
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // "many-to-many-polymorphic" relationship.
    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }

    // "polymorphic" relationship.
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
