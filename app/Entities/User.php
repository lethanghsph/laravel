<?php

namespace App\Entities;

use App\Entities\Product;
use App\Notifications\MailResetPasswordToken;
use Illuminate\Auth\Passwords\CanResetPassword as CanResetPasswordTrait;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use NF\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use NF\Roles\Models\Role;
use NF\Roles\Traits\HasRoleAndPermission;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable implements HasRoleAndPermissionContract, CanResetPassword
{
    use Notifiable;
    use TransformableTrait;
    use HasRoleAndPermission;
    use CanResetPasswordTrait;

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE  = 1;

    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'phone_number',
        'avatar',
        'gender',
        'description',
    ];

    /**
     * Use to custom view in reset password mail
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordToken($token));
    }

    public function getToken()
    {
        return JWTAuth::fromUser($this);
    }

    // "many-many" relationship.
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    // "many-many" relationship.
    public function cart()
    {
        return $this->belongsToMany(Product::class, 'carts', 'user_id', 'product_id');
    }
}
