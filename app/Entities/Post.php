<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\Comment;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post.
 *
 * @package namespace App\Entities;
 */
class Post extends Model
{
    const STATUS_DRAF    = 0;
    const STATUS_PENDING = 1;
    const STATUS_ACTIVE  = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'image',
        'content',
    ];

    // "one-many" relationship.
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // "many-to-many-polymorphic" relationship.
    public function categories()
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }

    // "polymorphic" relationship.
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
