<?php

namespace App\Entities;

use App\Entities\Post;
use App\Entities\Product;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category.
 *
 * @package namespace App\Entities;
 */
class Category extends Model
{

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE  = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    // "many-to-many-polymorphic" relationship.
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'categoriable');
    }

    // "many-to-many-polymorphic" relationship.
    public function products()
    {
        return $this->morphedByMany(Product::class, 'categoriable');
    }
}
