<?php

namespace App\Transformers;

use App\Entities\Product;
use App\Transformers\CategoryTransformer;
use App\Transformers\CommentTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class ProductTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProductTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users',
        'categories',
        'comments',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the Product entity.
     *
     * @param \App\Entities\Product $model
     *
     * @return array
     */
    public function transform(Product $model)
    {
        return [
            'id'         => (int) $model->id,
            'title'      => $model->title,
            'slug'       => $model->slug,
            'thumbnail'  => $model->thumbnail,
            'content'    => $model->content,
            'price'      => $model->price,
            'user_id'    => $model->user_id,
            'status'     => $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    public function includeUser(Product $model)
    {
        if (!empty($model->user)) {
            return $this->item($model->user, new UserTransformer);
        }
    }

    public function includeCategories(Product $model)
    {
        if (!empty($model->categories)) {
            return $this->collection($model->categories, new CategoryTransformer);
        }
    }

    public function includeComments(Product $model)
    {
        if (!empty($model->comments)) {
            return $this->collection($model->comments, new CommentTransformer);
        }
    }
}
