<?php

namespace App\Transformers;

use App\Entities\User;
use App\Transformers\PostTransformer;
use App\Transformers\ProductTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
        'posts',
        'products',
        'carts',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the User entity.
     *
     * @param \App\Entities\User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'           => (int) $model->id,
            'email'        => $model->email,
            'first_name'   => $model->first_name,
            'last_name'    => $model->last_name,
            'avatar'       => $model->avatar,
            'gender'       => $model->gender,
            'phone_number' => $model->phone_number,
            'description'  => $model->description,
            'status'       => $model->status,
            'created_at'   => $model->created_at,
            'updated_at'   => $model->updated_at,
        ];
    }

    public function includeRoles(User $model)
    {
        return $this->collection($model->roles, new RoleTransformer);
    }

    public function includePosts(User $model)
    {
        if (!empty($model->posts)) {
            return $this->collection($model->posts, new PostTransformer);
        }
    }

    public function includeProducts(User $model)
    {
        if (!empty($model->products)) {
            return $this->collection($model->products, new ProductTransformer);
        }
    }

    public function includeCart(User $model)
    {
        if (!empty($model->cart)) {
            return $this->collection($model->cart, new ProductTransformer);
        }
    }
}
