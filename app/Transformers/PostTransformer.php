<?php

namespace App\Transformers;

use App\Entities\Post;
use App\Transformers\CategoryTransformer;
use App\Transformers\CommentTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer.
 *
 * @package namespace App\Transformers;
 */
class PostTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'users',
        'categories',
        'comments',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the Post entity.
     *
     * @param \App\Entities\Post $model
     *
     * @return array
     */
    public function transform(Post $model)
    {
        return [
            'id'         => (int) $model->id,
            'title'      => $model->title,
            'slug'       => $model->slug,
            'image'      => $model->image,
            'content'    => $model->content,
            'user_id'    => (int) $model->user_id,
            'status'     => $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    public function includeUser(Post $model)
    {
        if (!empty($model->user)) {
            return $this->item($model->user, new UserTransformer);
        }
    }

    public function includeCategories(Post $model)
    {
        if (!empty($model->categories)) {
            return $this->collection($model->categories, new CategoryTransformer);
        }
    }

    public function includeComments(Post $model)
    {
        if (!empty($model->comments)) {
            return $this->collection($model->comments, new CommentTransformer);
        }
    }
}
