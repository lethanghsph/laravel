<?php

namespace App\Transformers;

use App\Entities\Comment;
use League\Fractal\TransformerAbstract;

/**
 * Class CommentTransformer.
 *
 * @package namespace App\Transformers;
 */
class CommentTransformer extends TransformerAbstract
{
    /**
     * Transform the Comment entity.
     *
     * @param \App\Entities\Comment $model
     *
     * @return array
     */
    public function transform(Comment $model)
    {
        return [
            'id'               => (int) $model->id,
            'body'             => $model->body,
            'commentable_id'   => $model->commentable_id,
            'commentable_type' => $model->commentable_type,
        ];
    }
}
