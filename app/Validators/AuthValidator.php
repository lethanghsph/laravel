<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class AuthValidator extends AbstractValidator
{

    protected $rules = [
        'LOGIN' => [
            'email'    => ['required', 'email'],
            'password' => ['required', 'min:4'],
        ],
    ];
}
