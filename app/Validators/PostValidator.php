<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

/**
 * Class PostValidator.
 *
 * @package namespace App\Validators;
 */
class PostValidator extends AbstractValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE             => [
            'title' => ['required'],
        ],
        ValidatorInterface::RULE_UPDATE             => [
        ],
        ValidatorInterface::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        ValidatorInterface::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
        'IMAGES'                                    => [
            'url' => ['required', 'url'],
        ],
    ];
}
