<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

/**
 * Class UserValidator.
 *
 * @package namespace App\Validators;
 */
class UserValidator extends AbstractValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        'ADMIN_CREATE_USER' => [
            'email'        => ['required', 'email'],
            'password'     => ['required', 'min:6', 'max:30'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'role'         => ['required'],
        ],
        'ADMIN_UPDATE_USER' => [
            'email'        => ['required', 'email'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
        ],
        'RULE_CREATE'       => [
            'email'        => ['required', 'email'],
            'password'     => ['required', 'min:6', 'max:30'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
            'role'         => ['required'],
        ],
        'RULE_UPDATE'       => [
            'email'        => ['required', 'email'],
            'first_name'   => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'    => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'phone_number' => ['required', 'regex:/^\d+$/', 'max:15'],
        ],
        'CHANGE_PASSWORD'   => [
            'email'        => ['required'],
            'old_password' => ['required'],
            'new_password' => ['required'],
        ],
        'UPDATE_PASSWORD'   => [
            'email'    => ['required'],
            'password' => ['required'],
        ],
        'IMAGES'            => [
            'url' => ['required', 'url'],
        ],
        'VERIFY_EMAIL'      => [
            'token' => ['required'],
        ],
        'UPDATE_ROLE'       => [
            'role' => ['required', 'regex:/[customer|artist]/'],
        ],
    ];
}
