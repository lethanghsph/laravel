<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

/**
 * Class CommentValidator.
 *
 * @package namespace App\Validators;
 */
class CommentValidator extends AbstractValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE             => [
            'body' => ['required'],
        ],
        ValidatorInterface::RULE_UPDATE             => [
            'body' => ['required'],
        ],
        ValidatorInterface::CHANGE_STATUS_ALL_ITEMS => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
        ValidatorInterface::CHANGE_STATUS_ITEM      => [
            'status' => ['required'],
        ],
    ];
}
