<?php

namespace App\Http\Controllers\Api\Admin;

use App\Entities\Artist;
use App\Entities\Customer;
use App\Entities\Export;
use App\Entities\Product;
use App\Entities\User;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\FileController;
use App\Http\Controllers\ExportController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        // $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new User;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('first_name', 'like', "%{$search}%");
                $q->orWhere('last_name', 'like', "%{$search}%");
                $q->orWhere('email', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'desc');
        }

        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $users    = $query->paginate($per_page);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->paginator($users, $transformer);
    }

    public function getAll(Request $request)
    {
        $query = new User;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('first_name', 'like', "%{$search}%");
                $q->orWhere('last_name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }
        $users = $query->get();

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }
        return $this->collection($users, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.users')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'ADMIN_CREATE_USER');
        if (!$this->repository->skipCache()->findByField('email', $request->get('email'))->isEmpty()) {
            throw new ConflictHttpException('Email already exist', null, 1001);
        }
        $data                 = $request->all();
        $user                 = $this->repository->create($data);
        $user->password       = Hash::make($data['password']);
        $user->email_verified = 1;
        $user->status         = User::STATUS_ACTIVE;
        $user->save();

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = $this->repository->find($id);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new UserTransformer;
        }

        return $this->response->item($user, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.users')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'ADMIN_UPDATE_USER');
        $existsEmail = $this->repository->existsEmail($id, $request->get('email'));
        if ($existsEmail) {
            throw new ConflictHttpException("Email has exists", null, 1001);
        }
        $attributes = $request->all();

        $user = $this->repository->update($attributes, $id);

        if ($request->has('status')) {
            $user->status = $request->get('status');
            $user->save();
        }

        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if (!Auth::user()->can('delete.users')) {
        //     throw new PermissionDeniedException;
        // }
        // $user = $this->repository->find($id);
        // if ($user->isRole('admin')) {
        //     throw new PermissionDeniedException;
        // }

        $this->repository->delete($id);
        return $this->success();
    }

    public function updatePassword(Request $request)
    {
        $this->validator->isValid($request, 'UPDATE_PASSWORD');
        $user           = $this->repository->findByField('email', $request->get('email'))->first();
        $user->password = Hash::make($request->get('password'));
        $user->save();

        return $this->success();
    }

    public function images(Request $request, $id)
    {
        if (!Auth::user()->can('update.users')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'IMAGES');

        $user         = $this->repository->find($id);
        $user->avatar = $request->get('url');
        $user->save();
        return $this->success();
    }
}
