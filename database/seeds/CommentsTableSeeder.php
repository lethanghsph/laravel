<?php

use App\Entities\Comment;
use App\Entities\Post;
use App\Entities\Product;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::all();
        $products = Product::all();

        factory(Comment::class, 50)->make()->each(function ($comment) use ($posts) {
            $posts->random()->comments()->save($comment);
        });

        factory(Comment::class, 50)->make()->each(function ($comment) use ($products) {
            $products->random()->comments()->save($comment);
        });
    }
}
