<?php

use App\Entities\Category;
use App\Entities\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all(['id']);

        factory(Product::class, 20)->create()->each(function ($product) use ($categories) {
            $product->categories()->sync([$categories->random()->id]);
        });
    }
}
