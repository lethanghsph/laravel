<?php

use App\Entities\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $adminRole    = Role::find(1);

        $users = [
            'admin' => [
                'first_name' => 'Admin',
                'last_name'  => '',
                'email'      => 'admin@localhost.test',
                'role'       => 'adminRole',
            ],
            'admin' => [
                'first_name' => 'Admin 2',
                'last_name'  => '',
                'email'      => 'admin2@localhost.test',
                'role'       => 'adminRole',
            ],
        ];

        foreach ($users as $item) {
            $user_data = [
                'email'      => $item['email'],
                'first_name' => $item['first_name'],
                'last_name'  => $item['last_name'],
            ];

            $user = User::updateOrCreate([
                'email' => $item['email'],
            ], $user_data);

            $user->password     = Hash::make('secret');
            $user->avatar       = '/assets/images/avatar.png';
            $user->status       = User::STATUS_ACTIVE;
            $user->gender       = 1;
            $user->phone_number = $faker->phoneNumber;
            $user->description  = $faker->paragraphs(3, true);
            $user->save();
            $user->attachRole(${$item['role']});
        }
    }
}
