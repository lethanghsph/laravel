<?php

use App\Entities\Product;
use App\Entities\User;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $user  = User::where('email', 'admin@localhost.test')->first();
    $title = 'Product ' . $faker->unique()->numberBetween(1, 50);

    return [
        'title'     => $title,
        'slug'      => str_slug($title),
        'thumbnail' => '',
        'content'   => $faker->paragraphs(100, true),
        'price'     => $faker->randomNumber(2),
        'status'    => Product::STATUS_ACTIVE,
        'user_id'   => $user->id,
    ];
});
