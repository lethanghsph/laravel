<?php

use App\Entities\Post;
use App\Entities\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    $user  = User::where('email', 'admin@localhost.test')->first();
    $title = 'Post ' . $faker->unique()->numberBetween(1, 50);

    return [
        'title'     => $title,
        'slug'      => str_slug($title),
        'thumbnail' => '',
        'content'   => $faker->paragraphs(100, true),
        'status'    => Post::STATUS_ACTIVE,
        'user_id'   => $user->id,
    ];
});
