<?php

use App\Entities\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $title = 'Category ' . $faker->unique()->numberBetween(1, 10);
    return [
        'title'  => $title,
        'slug'   => str_slug($title),
        'status' => Category::STATUS_ACTIVE,
    ];
});
