export const environment = {
	production: false,
	apiUrl: "http://localhost.test:8000",
	JWT_TOKEN_KEY: "jwt_token_key",
	user: null
};
