import { User } from "./user.model";

export interface Post {
    id: number;
    title: string;
    slug: string;
    image: string;
    content: string;
    status: number;
    user_id: number;
    created_at: string;
    updated_at: string;
    user?: User
}