export interface User {
    id: number;
    email: string;
    first_name: string;
    last_name: string;
    avatar: string;
    gender;
    phone_number: string;
    description: string;
    status: number;
    created_at: string;
    updated_at: string;
}