import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  menus = [
    {
      label: "Users",
      url: "/users",
      icon: "fa fa-archive"
    },
    {
      label: "Categories",
      url: "/categories",
      icon: "fa fa-archive"
    },
    {
      label: "Products",
      url: "/products",
      icon: "fa fa-archive"
    },
    {
      label: "Orders",
      url: "/orders",
      icon: "fa fa-archive"
    },
    {
      label: "Posts",
      url: "/posts",
      icon: "fa fa-archive"
    },
    {
      label: "Testimonials",
      url: "/testimonials",
      icon: "fa fa-archive"
    },
  ];
  
  constructor() { }

  ngOnInit() {
  }

}
