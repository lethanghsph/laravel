import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PostModule } from './post/post.module';
import { MainComponent } from './main.component';
import { CategoryModule } from './category/category.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [DashboardComponent, MainComponent]
})
export class ComponentsModule { }
