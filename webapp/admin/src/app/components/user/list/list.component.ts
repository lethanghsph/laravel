import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../api';
import { User, Pagination } from '../../../models';

import * as _ from 'lodash';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  users: User[];
  pagination: Pagination;
  isLoading: boolean = false;
  

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void
  {
    this.isLoading = true;

    this.userService.query().subscribe(data => {
      this.isLoading = false;
      this.users = data.users;
      this.pagination = data.pagination;
      console.log(this.users);
    });
  }

  deleteUser(user)
  {
    this.userService.delete(user.id).subscribe(
      response => {
        _.remove(this.users, user);
      }
    )
  }


}
