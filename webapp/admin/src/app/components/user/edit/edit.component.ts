import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';

import { UserService } from '../../../api';
import { User } from '../../../models';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  id = this.route.snapshot.paramMap.get('id');
  userForm: FormGroup;

  user: User = {} as User;
  isLoading: boolean = false;
  isSubmitting: boolean = false;
  
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void
  {
    this.isLoading = true;
    this.userService.show(this.id).subscribe(
      res => {
        this.isLoading = false;
        this.user = res;
        this.userForm.patchValue(res);
      }
    )
  }

  onSubmit(user)
  {
    this.isLoading = true;
    this.updateUser(this.userForm.value);
    this.userService.update(this.id, this.user).subscribe(
      res => {
        this.isLoading = false;
        this.user = res;
      }
    )
  }

  updateUser(value: User)
  {
    Object.assign(this.user, value);
  }

  createForm(): void
  {
    this.userForm = this.formBuilder.group({
      email: [''],
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      avatar: ['', [Validators.required]],
      gender: [''],
      phone_number: [''],
      description: [''],
      status: ['', [Validators.required]],
      array_test: this.formBuilder.array([])
    });
  }

  get arrayTest() {
    return this.userForm.get('array_test') as FormArray;
  }

  addArrayItem(): void
  {
    const itemArray = this.formBuilder.group({
      item_1: [],
      item_2: [],
      item_3: []
    });
    this.arrayTest.push(itemArray);
  }

  removeArrayItem(i)
  {
    this.arrayTest.removeAt(i);
  }
}
