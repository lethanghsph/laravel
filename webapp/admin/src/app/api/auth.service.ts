import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { User } from '../models';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  private login_path = '/api/admin/login';

  constructor(
    private apiService: ApiService,
    private jwtService: JwtService
  ) { }

  setAuth(user: User, token)
  {
    this.jwtService.saveToken(token);
    this.currentUserSubject.next(user);
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth()
  {
    this.jwtService.destroyToken();
    this.currentUserSubject.next({} as User);
    this.isAuthenticatedSubject.next(false);
  }

  attemptAuth(credentials)
  {
    return this.apiService.post(this.login_path, credentials)
      .pipe(map(
        data => {
          this.setAuth(data.user, data.token);
          return data;
        }
      ))
  }

  getCurrentUser(): User
  {
    return this.currentUserSubject.value;
  }
}
