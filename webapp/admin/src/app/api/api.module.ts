import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './user.service';
import { JwtService } from './jwt.service';
import { ApiService } from './api.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ApiService,
    UserService,
    JwtService
  ]
})
export class ApiModule { }
