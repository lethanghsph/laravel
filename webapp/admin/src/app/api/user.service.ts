import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { UserTransformer, PaginationTransformer } from '../transfomers';
import { map } from 'rxjs/operators';

import * as _ from 'lodash';
import { HttpResponseBase } from '../../../node_modules/@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private path = '/api/admin/users';

  constructor(
    private apiService: ApiService
  ) { }

  query(param: string = ""): Observable<any>
  {
    return this.apiService.get(this.path + param).pipe(
      map(function(response){
        let data = {
          users: _.map(response.data, item => new UserTransformer(item)),
          pagination: new PaginationTransformer(response.meta.pagination)
        };
        return data;
      })
    );
  }

  show(id, param: string = ""): Observable<any>
  {
    return this.apiService.get(this.path + "/" + id + param).pipe(
      map(response => new UserTransformer(response.data))
    );
  }

  store(item: Object): Observable<any>
  {
    return this.apiService.post(this.path, item).pipe(
      map(response => new UserTransformer(response.data))
    )
  }

  update(id, item: Object): Observable<any>
  {
    return this.apiService.put(this.path + "/" + id, item).pipe(
      map(response => new UserTransformer(response.data))
    )
  }

  delete(id): Observable<any>
  {
    return this.apiService.delete(this.path + "/" + id);
  }
}
