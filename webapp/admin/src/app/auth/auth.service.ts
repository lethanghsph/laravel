import { Injectable } from '@angular/core';
import * as _ from "lodash";
import * as Cookies from "js-cookie";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isAuthenticated(): boolean {
    const token = Cookies.get(environment.JWT_TOKEN_KEY);
    return !_.isNil(token);
  }
}
