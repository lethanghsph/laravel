import { environment } from '../../environments/environment';
import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { Observable } from "rxjs";
import * as Cookies from "js-cookie";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        "Content-Type": "application/json",
        "Authorization": 'Bearer ' + Cookies.get('jwt_token_key')
      }
    });
    return next.handle(request);
  }
}
