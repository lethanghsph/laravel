import { Transformer } from "./transfomer";

export class UserTransformer extends Transformer {

    transform(data) {
        let transfomer = <any>{};
        
        transfomer = {
            id: data.id,
            email: data.email,
            first_name: data.first_name,
            last_name: data.last_name,
            name: data.last_name + ' ' + data.first_name,
            avatar: data.avatar,
            gender: data.gender,
            phone_number: data.phone_number,
            description: data.description,
            status: data.status,
            created_at: data.created_at,
            updated_at: data.updated_at,
        };

        return transfomer;
    }
}