export class Transformer {

    constructor(options)
    {
        let ownProperties = this.transform(options);
        this.setProperties(ownProperties);
    }

    setProperties(ownProperties) {
        Object.keys(ownProperties).forEach(function(k){
            this[k] = ownProperties[k];
        }, this);
    }

    transform(model: {} = {}): {}
    {
        return {};
    }
}
