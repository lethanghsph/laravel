import { Transformer } from "./transfomer";

export class PaginationTransformer extends Transformer {
    
    transform(data) {
        let transfomer = <any>{};

        transfomer = {
            total: data.total,
            count: data.count,
            per_page: data.per_page,
            current_page: data.current_page,
            total_pages: data.total_pages,
            links: data.links,
        };

        return transfomer;
    }

}