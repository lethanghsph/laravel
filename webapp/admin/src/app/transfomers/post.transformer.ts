import { Transformer } from "./transfomer";
import { UserTransformer } from "./user.transformer";

export class PostTransfromer extends Transformer {

    transform(data) {
        let transform = <any>{};
        
        transform = {
            id: data.id,
            title: data.title,
            slug: data.slug,
            image: data.image,
            content: data.content,
            status: data.status,
            user_id: data.user_id,
            created_at: data.created_at,
            updated_at: data.updated_at,
        };

        if(undefined !== data.users.data && Array.isArray(data.users.data)) {
            transform.user = new UserTransformer(data.users.data);
        }

        return transform;
    }
}