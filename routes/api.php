<?php

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

    /**
     * Admin API
     */
    $api->group(['prefix' => 'admin'], function ($api) {
        // Auth API
        $api->post('login', 'App\Http\Controllers\Api\Admin\AuthController@authenticate');
        $api->get('me', 'App\Http\Controllers\Api\Admin\AuthController@me');

        $api->get('users/all', 'App\Http\Controllers\Api\Admin\UserController@getAll');
        $api->resource('users', 'App\Http\Controllers\Api\Admin\UserController');
    });
});
